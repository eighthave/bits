Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2015)
Slug: new-developers-2015-12
Date: 2016-01-12 12:30
Author: Jean-Pierre Giraud
Translator: Adrià García-Alzórriz
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

  * Stein Magnus Jodal (jodal)
  * Prach Pongpanich (prach)
  * Markus Koschany (apo)
  * Bernhard Schmidt (berni)
  * Uwe Kleine-König (ukleinek)
  * Timo Weingärtner (tiwe)
  * Sebastian Andrzej Siewior (bigeasy)
  * Mattia Rizzolo (mattia)
  * Alexandre Viau (aviau)
  * Lev Lamberov (dogsleg)
  * Adam Borowski (kilobyte)
  * Chris Boot (bootc)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

  * Alf Gaida
  * Andrew Ayer
  * Marcio de Souza Oliveira
  * Alexandre Detiste
  * Dave Hibberd
  * Andreas Boll
  * Punit Agrawal
  * Edward Betts
  * Shih-Yuan Lee
  * Ivan Udovichenko
  * Andrew Kelley
  * Benda Xu
  * Russell Sim
  * Paulo Roberto Alves de Oliveira
  * Marc Fournier
  * Scott Talbert
  * Sergio Durigan Junior
  * Guillaume Turri
  * Michael Lustfield

Enhorabona a tots!
