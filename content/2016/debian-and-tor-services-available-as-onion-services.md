Title: Debian and Tor Services available as Onion Services
Slug: debian-and-tor-services-available-as-onion-services
Date: 2016-08-01 17:30
Author: Peter Palfrader
Tags: tor, tor network, onion services
Status: published

We, the [Debian project][debian] and the [Tor project][tor] are enabling
Tor [onion services][onion] for several of our sites. These sites can now be
reached without leaving the Tor network, providing a new option for
securely connecting to resources provided by Debian and Tor.

The freedom to use open source software may be compromised when access to that
software is monitored, logged, limited, prevented, or prohibited. As a
community, we acknowledge that users should not feel that their every action
is trackable or observable by others. Consequently, we are pleased to announce
that we have started making several of the various web services provided by
both Debian and Tor available via onion services.

While onion services can be used to conceal the network location of the machine
providing the service, this is not the goal here. Instead, we employ onion
services because they provide end-to-end integrity and confidentiality,
and they authenticate the onion service end point.

For instance, when users connect to the onion service running at
[http://sejnfjrq6szgca7v.onion/](http://sejnfjrq6szgca7v.onion/),
using a Tor-enabled browser such as the [TorBrowser][tb],
they can be certain that their connection to the [Debian website][debian] cannot
be read or modified by third parties, and that the website that they are
visiting is indeed the Debian website. In a sense, this
is similar to what using [HTTPS][https] provides. However, crucially, onion
services do not rely on third-party [certification authorities (CAs)][CA].
Instead, the onion service name cryptographically authenticates its
cryptographic key.

In addition to the Tor and Debian websites, the [Debian FTP][ftp] and the [Debian
Security][security] archives are available from .onion addresses, enabling
Debian users to update their systems using only Tor connections. With the
[apt-transport-tor package][att] installed, the following entries can replace
the normal debian mirror entries in the apt configuration file (`/etc/apt/sources.list`):

      deb  tor+http://vwakviie2ienjx6t.onion/debian          jessie            main
      deb  tor+http://vwakviie2ienjx6t.onion/debian          jessie-updates    main
      deb  tor+http://sgvtcaew4bxjd7ln.onion/debian-security jessie/updates    main

Likewise, [Tor's Debian package repository][tordeb] is available from an onion service :

      deb tor+http://sdscoq7snqtznauu.onion/torproject.org   jessie    main

Where appropriate, we provide services redundantly from several backend machines
using [OnionBalance][ob]. The [Debian OnionBalance package][obd] is available
from the [Debian backports repository][bpo].

Lists of several other new onion services offered by Debian and Tor are
available from [https://onion.debian.org](https://onion.debian.org) and
[https://onion.torproject.org](https://onion.torproject.org) respectively.
We expect to expand these lists in the near future to cover even more
of Debian's and Tor's services.

[bpo]: https://backports.debian.org/
[debian]: https://www.debian.org/
[tor]: https://www.torproject.org/
[onion]: https://www.torproject.org/docs/hidden-services
[tb]: https://www.torproject.org/projects/torbrowser
[https]: https://en.wikipedia.org/wiki/HTTPS
[CA]: https://en.wikipedia.org/wiki/Certificate_authority
[ftp]: http://ftp.debian.org/
[security]: http://security.debian.org/debian-security
[att]: https://packages.debian.org/apt-transport-tor
[ob]: https://github.com/DonnchaC/onionbalance
[obd]: https://packages.debian.org/onionbalance
[tordeb]: https://www.torproject.org/docs/debian
